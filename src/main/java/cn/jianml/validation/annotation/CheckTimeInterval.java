package cn.jianml.validation.annotation;

import cn.jianml.validation.validator.CheckTimeIntervalValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = CheckTimeIntervalValidator.class)
public @interface CheckTimeInterval {

    String startTime() default "from";

    String endTime() default "to";

    String message() default "开始时间不能大于结束时间";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
