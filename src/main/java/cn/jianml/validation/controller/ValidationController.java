package cn.jianml.validation.controller;

import cn.jianml.validation.annotation.IsSex;
import cn.jianml.validation.entity.Book;
import cn.jianml.validation.entity.Coupon;
import cn.jianml.validation.entity.User;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author wujian
 * @time 2020/1/4
 */
@RestController
@Validated
public class ValidationController {

    @GetMapping("/validate1")
    public String validate1(
            @Size(min = 1, max = 10, message = "姓名长度必须为1到10") @RequestParam("name") String name,
            @Min(value = 10, message = "年龄最小为10") @Max(value = 100, message = "年龄最大为100")  @RequestParam("age") Integer age,
            @Future @RequestParam("birth") @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss") Date birth){
        return "validate1";
    }

    @PostMapping("/validate2")
    public User validate2(@Valid @RequestBody User user){
        return user;
    }

    @GetMapping("/validate3")
    public String validate3(
            @Size(min = 1, max = 10, message = "姓名长度必须为1到10") @RequestParam("name") String name,
            @Min(value = 10, message = "年龄最小为10") @Max(value = 100, message = "年龄最大为100")  @RequestParam("age") Integer age,
            @Future @RequestParam("birth") @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss") Date birth,
            @IsSex(values = "0,1") @RequestParam("sex") String sex){
        return "validate3";
    }

    @GetMapping("/validate4")
    public String validate4(@Valid @RequestBody Book book){
        return "validate4";
    }

    @GetMapping("/validate5")
    public String validate5(@Valid @RequestBody Coupon coupon){
        return "validate5";
    }
}
