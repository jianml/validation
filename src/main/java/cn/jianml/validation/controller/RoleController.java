package cn.jianml.validation.controller;

import cn.jianml.validation.entity.Role;
import cn.jianml.validation.entity.WebResult;
import cn.jianml.validation.service.Add;
import cn.jianml.validation.service.Update;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wujian
 * @time 2020/1/6
 */
@RestController
public class RoleController {

    @PostMapping("/addRole")
    public WebResult addRole(@Validated(Add.class) Role role){
        return new WebResult().msg("添加成功");
    }

    @PutMapping("/updateRole")
    public WebResult updateRole(@Validated(Update.class) Role role){
        return new WebResult().msg("修改成功");
    }
}
