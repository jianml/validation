package cn.jianml.validation.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * @author wujian
 * @time 2020/1/6
 */
@Data
public class Book {

    @NotBlank(message = "name不允许为空")
    @Length(min = 2, max = 10, message = "name的长度必须在{min}-{max}之间")
    private String name;

    @Valid
    private User author;
}
