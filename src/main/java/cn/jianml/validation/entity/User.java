package cn.jianml.validation.entity;

import cn.jianml.validation.annotation.IsSex;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author wujian
 * @time 2020/1/4
 */
@Data
public class User {

    @Size(min = 1, max = 10, message = "姓名长度必须为1到10")
    private String name;

    @Min(value = 10, message = "年龄最小为10")
    @Max(value = 100, message = "年龄最大为100")
    private Integer age;

    @Future
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birth;

    @IsSex(values = "0,1")
    private String sex;
}
