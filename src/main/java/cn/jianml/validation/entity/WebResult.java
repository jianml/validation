package cn.jianml.validation.entity;

import java.util.HashMap;

/**
 * @author wujian
 * @time 2020/1/5
 */
public class WebResult extends HashMap<String, Object> {

    public WebResult msg(String msg){
        put("msg", msg);
        return this;
    }

    public WebResult data(Object data){
        put("data", data);
        return this;
    }


}
