package cn.jianml.validation.entity;

import cn.jianml.validation.annotation.CheckTimeInterval;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 优惠券
 * @author wujian
 * @time 2020/1/6
 */
@CheckTimeInterval(startTime = "releaseStartTime", endTime = "releaseEndTime", message = "发放开始时间不能大于发放结束时间")
@Data
public class Coupon {

    @NotBlank(message = "name不允许为空")
    @Length(min = 2, max = 10, message = "name的长度必须在{min}-{max}之间")
    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date releaseStartTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date releaseEndTime;

}
