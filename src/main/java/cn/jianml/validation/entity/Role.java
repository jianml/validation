package cn.jianml.validation.entity;

import cn.jianml.validation.service.Add;
import cn.jianml.validation.service.Update;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author wujian
 * @time 2020/1/6
 */
@Data
public class Role {

     // 修改角色时，必须要有id
    @NotNull(message = "修改角色必须要有id", groups = Update.class)
    private Long id;

    // 添加角色时必须要有name
    @NotNull(message = "添加角色时必须要有name", groups = Add.class)
    // 添加修改角色都需要name的长度在1-10
    @Length(min = 1, max = 10, message = "名称不合法", groups = {Add.class, Update.class})
    private String name;
}
