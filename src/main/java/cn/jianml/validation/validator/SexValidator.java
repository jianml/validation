package cn.jianml.validation.validator;

import cn.jianml.validation.annotation.IsSex;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 性别验证处理类
 * @author wujian
 * @time 2020/1/5
 */
public class SexValidator implements ConstraintValidator<IsSex, Object> {

    /**
     * IsSex注解规定的那些有效值
     */
    private String values;

    @Override
    public void initialize(IsSex constraintAnnotation) {
        this.values = constraintAnnotation.values();
    }

    /**
     * 用户输入的值，必须是IsSex注解规定的那些值其中之一。
     * 否则，校验不通过。
     * @param o 用户输入的值，如从前端传入的某个值
     */
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        String[] sexs = values.split(",");
        for(String sex : sexs){
            if(sex.equals(o))
                return true;
        }
        return false;
    }
}
